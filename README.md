# About

This project is a simulation engine to run geometry based evolutionary 
repeated game. Currently only Prisoner's Dilemma is supported. 

Engine is written in Kotlin with UI for two architectures: desktop (java) 
and browser (javascript). 

# Prisoner's Dilemma

https://en.wikipedia.org/wiki/Prisoner%27s_dilemma 

# Demo

js version on browser: https://strwrite.gitlab.io/trustmatrix

java version: https://gitlab.com/strwrite/trustmatrix/-/jobs/81114273/artifacts/download

Strategies:
 - White is an "always cooperates"
 - Black is an "always cheats"
 - Blue is an "eye for an eye"
 - Yellow is a "detective"

"Always cheats/cooperates" just always cheats/cooperates.
"Eye for an eye" firstly cooperates, then every move just repeates 
opponent's previous move. 
"Detective" assumes that it knows every other strategy presented and 
for first two moves tries to detect who is playing against. 
Then it maximizes it's own outcome - cheats on "always cheats/cooperates", 
be friends with "eye for an eye" and itself.

